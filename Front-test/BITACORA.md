_____________________________________________________________________________________________________________________________________________________________________

FRONT END TEST 
_____________________________________________________________________________________________________________________________________________________________________

DESCRIPTION
_____________________________________________________________________________________________________________________________________________________________________

    Este test para el desarrollo front end está creado para valorar las habilidades técnicas, por ende se plantean diversos casos en los que se deberán responder una
    serie de requerimientos, estos requerimientos van desde habilidades con desarrollo visual como operativo y según en cumplimiento de parámetros se valorará la 
    calidad de este para ser seleccionado.
_____________________________________________________________________________________________________________________________________________________________________

PREREQUISITES
_____________________________________________________________________________________________________________________________________________________________________

    Node 16.13.1
_____________________________________________________________________________________________________________________________________________________________________

INSTALLATION
_____________________________________________________________________________________________________________________________________________________________________

    Inicalmente seguiremos los pasos indicados en la instalación default para la prueba

    - En la carpeta root, en este caso en FRONT-TEST en la terminal corremos la indicación |npm install|
    - usando cd front, nos encontramos en front y usamos el comando en el terminal llamado |npm install|

    Con estos pasos concluímos con la instalación
_____________________________________________________________________________________________________________________________________________________________________

BITÁCORA
_____________________________________________________________________________________________________________________________________________________________________

    - Inicialmente corremos en la carpeta root el comando |npm run dev|.
    - Conociendo los requerimientos se plantéa una idea para el desarrollo lógico, estilo de desarrollo y el análisis para el uso correcto de la API.
    - Como se dijo anteriormente usaremos un estilo de desarrollo, para este caso es el estilo ATOM DESIGN que cumple con fracionar desde lo mas mínimo a lo mas 
    complejo, así que para nuestros componentes crearé sus respectivas carpetas (Atoms, molecules, organism, page).
    - Teniendo clara la idea necesitaremos las respectivas rutas para lo que será visible, en este caso fueron 3 pages visibles, las cuales fueron enrutadas en App.js.
    - Usando React-router-dom para así simplemente enrutar las tres páginas.
    - Analizando se concluye que la mejor opción será comenzar con la parte lógica y se comienza mostrando la información que necesitamos, para eso es que usaremos 
    Axios, Axios se encargará de comunicar el front con el back..
    - terminado de usar el axios y ahora con la información, resta comenzar a diseñar una manera cómoda para aplicar una buena visualización desde móvil.
    - luego de tener las vistas principales, se comienza a detallar el código.
    - se usa redux para el estado global de la compra y con esto avanzamos con casi todo cumplido y terminado.
    - se hacen pruebas para provocar fallos, encontrar errores de diseño y cumplir con buenas prácticas.
    - se finaliza la bitácora.

______________________________________________________________________________________________________________________________________________________________________