import React, { useState, useEffect } from 'react'
import Button from '../../atoms/button/button'
import Header from '../../molecules/header/header'
import ItemCart from '../../molecules/item-cart/itemCart'
import { useSelector } from 'react-redux'
import './shoppingCart.css'

const ShoppingCart = (props) => {
    const [total, setTotal] = useState(0)
    const products = useSelector(state => state.shoppingCartReducer.products)
    const getTotal = () => {
        let auxTotal = 0
        products.map(e => {
            auxTotal += e.price * e.cantidad
        })
        setTotal(auxTotal.toFixed(2))
    }
    useEffect(() => {
        getTotal()
    }, [])

    return (
        <div id="cart">
            <Header />
            <div className="payBill">
                <div className="box">
                    <h1 className="total">Total: ${total}</h1>
                </div>
                <div className="box">
                    <Button type="pay"/>
                </div>

            </div>
            {
                products.length > 0 ? products.map((e, k) => <ItemCart key={ k } _id={ e._id } image={e.image} name={e.name} price={e.price} stock={e.countInStock} cantidad={e.cantidad} />) : "no hay productos"
            }
        </div>
    )
}

export default ShoppingCart
