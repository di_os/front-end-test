import React, { useState, useEffect } from 'react'
import Cards from '../../molecules/cards/cards'
import './listProducts.css'
import Header from '../../molecules/header/header'
import Footer from '../../molecules/footer/footer'
import axios from 'axios'

const ListProducts = () => {
    const [ products, setProducts ] = useState([]);

    useEffect(() => {
        axios.get('http://localhost:5000/api/products')
            .then((response) => {
                setProducts(response.data)
            })
            .catch((error) => {
                console.log(error)
            })
    }, [])

    return (
        <div id="listProducts">
            <Header />
            <div className="container-cards">
                {
                    products.map((e, k) => 
                        <Cards 
                            key={k} 
                            image={e.image}
                            name={e.name} 
                            description={e.description} 
                            brand={e.brand} 
                            category={e.category} 
                            price={e.price} 
                            stock={e.countInStock} 
                            rating={e.rating} 
                            numReviews={e.numReviews}
                            _id={e._id}
                        />)
                }
            </div>
            <Footer/>
        </div>
    )
}

export default ListProducts
