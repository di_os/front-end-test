import React from 'react'
import './footer.css'

const Footer = () => {
    return (
        <div id="footer">
            <h3 className="textoFooter">2021 © Todos los derechos reservados</h3>
        </div>
    )
}
export default Footer