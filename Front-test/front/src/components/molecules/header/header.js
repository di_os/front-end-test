import React, { useEffect } from 'react'
import { Link } from 'react-router-dom'
import { useSelector } from 'react-redux'
import './header.css'

const Header = () => {
    const products = useSelector(state => state.shoppingCartReducer.products)

    return (
        <div id="header">
            <Link to="/" className="carrito"><img className="imagenHeader" src="https://www.ecomsur.com/wp-content/uploads/2020/02/LOGO-ECOMSUR-2019-2.png" /></Link>
            <Link className="carrito" to="/shopping-cart"><img className="imagenCarrito" src="https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ffindicons.com%2Ffiles%2Ficons%2F1700%2F2d%2F512%2Fcart.png&f=1&nofb=1" />{products.length}</Link>
        </div>
    )
}

export default Header