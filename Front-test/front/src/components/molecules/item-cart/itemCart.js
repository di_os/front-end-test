import React, { useState, useEffect } from "react";
import Button from "../../atoms/button/button";
import { updateCantidad } from "../../../store/shopping-cart/actions";
import { useDispatch } from "react-redux";
import './itemCart.css'

const ItemCart = (props) => {
    const [options, setOptions] = useState([])
    const dispatch = useDispatch();
    useEffect(() => {
        let auxOptions = []
        for (let i = 1; i <= props.stock; i++) {
            auxOptions.push(<option key={i}>{i}</option>)
        }
        setOptions(auxOptions)
    }, [])

    return (
        <div id="itemCart">
            <div className="show-product">
                <div className="leftInformation">
                    <img className="imagen" src={`http://localhost:5000/${props.image}`} />
                </div>
                <div className="show-description">
                    <h1 className="text-name">{props.name}</h1>
                    <div className="containerDesc">
                        <h3 className="text-price">Precio: {props.price}</h3>
                        <select className="selector" value={ props.cantidad } onChange={ e => dispatch(updateCantidad(props._id, e.target.value)) }>
                            {
                                options.map(e => e)
                            }
                        </select>
                    </div>
                    <Button tpye="remove" _id={props._id} />
                </div>
            </div>
        </div>
    )
}

export default ItemCart