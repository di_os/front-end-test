import { createStore, combineReducers } from 'redux';
import shoppingCartReducer from './shopping-cart/reducer';

const reducers = combineReducers({
    shoppingCartReducer
})

const store = createStore(
    reducers, 
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
)

export default store