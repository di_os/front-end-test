const addProduct = (product) => {
    return {
        type: 'ADD',
        payload: product
    }
}

const removeProduct = (id) => {
    return {
        type: 'REMOVE',
        payload: id
    }
}

const updateCantidad = (id, cantidad) => {
    return {
        type: 'UPDATE_CANTIDAD',
        payload: {
            id,
            cantidad
        }
    }
}

export {
    addProduct,
    removeProduct,
    updateCantidad
}