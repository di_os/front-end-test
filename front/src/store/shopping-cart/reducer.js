const initialState = {
    products: localStorage.getItem("shoppingcard") ? JSON.parse(localStorage.getItem("shoppingcard")) : []
}

export default (state = initialState, action) => {
    if (action.type === 'ADD') {
        let listProducts = localStorage.getItem("shoppingcard")
        let stop = false

        if (listProducts) {
            listProducts = JSON.parse(listProducts)
            listProducts.map(e => {
                if (e._id === action.payload._id) {
                    stop = true
                }
            })
        } else {
            listProducts = []
        }

        if (stop) {
            alert("Este producto ya se agregó a tu carrito")
            return state;
        }

        listProducts.push({
            "_id": action.payload._id,
            "name": action.payload.name,
            "image": action.payload.image,
            "description": action.payload.description,
            "brand": action.payload.brand,
            "category": action.payload.category,
            "price": action.payload.price,
            "countInStock": action.payload.stock,
            "rating": action.payload.rating,
            "numReviews": action.payload.numReviews,
            "cantidad": 1
        })
        localStorage.setItem("shoppingcard", JSON.stringify(listProducts))
        return {
            ...state.products,
            products: listProducts
        }
    }else if (action.type === 'REMOVE') {
        let listProducts = JSON.parse(localStorage.getItem("shoppingcard"));
        const newListProducts = listProducts.filter(e => e._id !== action.payload)
        localStorage.setItem("shoppingcard", JSON.stringify(newListProducts))
        return {
            products: newListProducts
        }
    }else if (action.type === 'UPDATE_CANTIDAD') {
        let listProducts = JSON.parse(localStorage.getItem("shoppingcard"));

        for (let i = 0; i < listProducts.length; i++) {
            if (listProducts[i]._id === action.payload.id) {
                listProducts[i].cantidad = action.payload.cantidad
            }
        }

        localStorage.setItem("shoppingcard", JSON.stringify(listProducts))

        return {
            products: listProducts
        }
    }
    return state
}