import React from 'react'
import { Rating } from 'react-simple-star-rating'
import Button from '../../atoms/button/button'
import './cards.css'

const Cards = (props) => {
    return (
        <div id="cards">
            <img className="productImage" src={`http://localhost:5000/${props.image}`} />
            <h1 className="textName">{props.name}</h1>
            <h4 className='textDesc'>{props.description}</h4>
            <div className='containerDesc'>
                <h4 className="textMarca">marca:</h4> <h3 className="textPrintMarca">{props.brand}</h3>
                <h4 className="textMarca">categoría:</h4> <h3 className="textPrintMarca">{props.category}</h3>
            </div>
            <div className="containerDesc">
                <h4 className="price">${props.price}</h4>
                <h4 className="stock">{props.stock} en stock</h4>
            </div>
            <div className="containerDesc">
                <Rating className="rating" initialValue={props.rating} readonly={true} />
                <h4 className="stockText">{props.numReviews}</h4><h4 className="stockText">valoraciones</h4>
            </div>
            <div className="container-buttons">
                {
                    props.stock > 0 &&
                    <Button type="comprar" data={ props } />
                }
                <Button type="Show" path={ `/show-product/${props._id}` } />
            </div>
        </div>
    )
}

export default Cards 