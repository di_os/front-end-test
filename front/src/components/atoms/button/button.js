import React from "react";
import { Link } from 'react-router-dom'
import { addProduct, removeProduct } from "../../../store/shopping-cart/actions"
import { useDispatch } from 'react-redux'
import './button.css'

const Button = (props) => {
    const dispatch = useDispatch();

    return (
        <div id="button">
            {
                props.type === "comprar" &&
                <button className="add" onClick={() => dispatch(addProduct(props.data))}>Añadir al carrito</button>
            }
            {
                props.type === "Show" &&
                <Link to={props.path}>
                    <button className="show-product">Mostrar el producto</button>
                </Link>
            }
            {
                props.type === "pay" &&
                <button className="pagar">Pagar</button>
            }
            {
                props.tpye === "remove" &&
                <button className="show-product" onClick={ () => dispatch(removeProduct(props._id)) } >x</button>
            }
        </div>
    )
}

export default Button