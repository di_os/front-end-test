import React, { useState, useEffect } from 'react'
import axios from 'axios'
import './showProduct.css'
import { Rating } from 'react-simple-star-rating'
import Header from '../../molecules/header/header'
import Footer from '../../molecules/footer/footer'
import Button from '../../atoms/button/button'

const ShowProduct = (props) => {
    const [product, setProduct] = useState({})

    useEffect(() => {
        axios.get(`http://localhost:5000/api/products/${props.match.params.id}`)
            .then((response) => {
                setProduct(response.data)
            })
            .catch((error) => {
                console.log(error)
            })
    }, [])

    return (
        <div id="show">
            <Header />
            <div className="show-product">
                <div className='leftInformation'>
                    <img className="imagen" src={`http://localhost:5000/${product.image}`} />
                    {
                        product.countInStock > 0 &&
                        <Button type="comprar" data={ product } />
                    }
                </div>
                <div className="show-description">
                    <h1 className="name">{product.name}</h1>
                    <h4 className="description">{product.description}</h4>
                    <div className='containerDesc'>
                        <h4 className="textMarca">marca:</h4> <h3 className="textPrintMarca">{product.brand}</h3>
                        <h4 className="textMarca">categoría:</h4> <h3 className="textPrintMarca">{product.category}</h3>
                    </div>
                    <div className="containerDesc">
                        <h4 className="price">${product.price}</h4>
                        <h4 className="stock">{product.countInStock} en stock</h4>
                    </div>
                    <div className="containerDesc">
                        <Rating className="rating" initialValue={product.rating} readonly={true} />
                        <h4 className="stockText">{product.numReviews}</h4><h4 className="stockText">valoraciones</h4>
                    </div>
                </div>
            </div>
            <Footer />
        </div>
    )
}

export default ShowProduct
