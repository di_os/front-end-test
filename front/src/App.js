import React, { useEffect, useState } from 'react'
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import ListProducts from './components/page/list-products/listProducts'
import ShowProduct from './components/page/show-product/showProduct'
import ShoppingCart from './components/page/shopping-cart/shoppingCart' 

const App = () => {
  return (
    <BrowserRouter>
      <Switch>
          <Route exact path="/" component={ ListProducts }/>
          <Route exact path="/show-product/:id" component={ ShowProduct }/>
          <Route exact path="/shopping-cart" component={ ShoppingCart }/>
      </Switch>
    </BrowserRouter>
  )
}

export default App
